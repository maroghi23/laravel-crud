<?php

namespace App\Http\Controllers;

use App\Models\Articles;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ArticlesController extends Controller
{
    public function index()
    {
        // get data
        $data = Articles::latest()->get();

        // make response JSON
        return response()->json([
            'success' => true,
            'message' => 'List Data',
            'data' => $data
        ], 200);
    }

    public function show($id)
    {
        // find by ID
        $dataByID = Articles::findOrFail($id);

        // make response JSON
        return response()->json([
            'success' => true,
            'message' => 'List Data',
            'data' => $dataByID
        ], 200);
    }

    public function store(Request $request)
    {
        // set validation
        $validator = Validator::make($request->all(), [
            'title' => 'required',
            'body'  => 'required',
            'file'  => 'mimes:jpeg,bmp,png,gif,svg,pdf|max:2048'
        ]);

        // response error validation
        if($validator->fails()) {          
            return response()->json($validator->errors(), 400);                        
        }

        // get value input
        $input = $request->all();

        // image validation
        if ($image = $request->file('file')) {
            $destinationPath = 'public/';
            $profileImage = date('YmdHis') . "." . $image->getClientOriginalExtension();
            $image->move($destinationPath, $profileImage);
            $input['file'] = $profileImage;
        }

        // save to database
        $data = Articles::create($input);

        // success save to database
        if($data) {

            return response()->json([
                'success' => true,
                'message' => 'Success to Save',
                'data'    => $data  
            ], 201);

        } 

        // failed save to database
        return response()->json([
            'success' => false,
            'message' => 'Failed to Save',
        ], 409);
    }

    public function update(Request $request, $id)
    {
        // return $request->all();
        // set validation
        $validator = Validator::make($request->all(), [
            'title' => 'required',
            'body'  => 'required',
        ]);
        
        // response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        // get value input
        $input = $request->all();

        // image validation
        if ($image = $request->file('file')) {
            $destinationPath = 'public/';
            $profileImage = date('YmdHis') . "." . $image->getClientOriginalExtension();
            $image->move($destinationPath, $profileImage);
            $input['file'] = $profileImage;
        }

        // find by ID
        $dataByID = Articles::findOrFail($id);

        if($dataByID) {

            // update dataByID
            $dataByID->update($input);

            return response()->json([
                'success' => true,
                'message' => 'Success to Updated',
                'data'    => $dataByID  
            ], 200);

        }

        // data not found
        return response()->json([
            'success' => false,
            'message' => 'Data Not Found',
        ], 404);

    }
    
    public function destroy($id)
    {
        // find by ID
        $dataByID = Articles::findOrfail($id);

        if($dataByID) {

            // delete dataByID
            $dataByID->delete();

            return response()->json([
                'success' => true,
                'message' => 'Success to Deleted',
            ], 200);

        }

        // data not found
        return response()->json([
            'success' => false,
            'message' => 'Data Not Found',
        ], 404);
    }
}
